# Stochastic filtering of GRACE and GRACE-FO data

This project contains MATLAB/Octave code for filtering GRACE and GRACE-FO data using the stochastic information of GRACE(-FO) monthly spherical harmonic solutions.