% Script for computing the Kaula rule coefficients

clc

more off

%fprintf('Reading GRACE coefficients\n')
%itsg = readitsg([data_path, '96/'], 'gfc');
%save([data_path, '96/ITSG2018_n96.mat'], '-v7', 'itsg');

fprintf('Loading GRACE data\n')
load([data_path, '96/ITSG2018_n96.mat']);
imean = getgracemean(itsg, [2005 1], [2010 12]);
itsg  = removetemporalmean(itsg, imean);

lmax  = size(itsg{1,9}, 1) - 1;

fprintf('Computing degree variances of the monthly mass anomalies\n')
dgvar        = cell(size(itsg,1),3);
dgvar(:,1:2) = itsg(:,4:5);
for k = 1:length(itsg)
    dgvar{k,3} = degvar(itsg{k,9},lmax);
end


fprintf('Computing power laws for the calendar months\n\n')
dgvarm      = cell(12,3);
m           = vertcat(dgvar{:,2});
mat         = zeros(lmax+1,13);
mat(:,1)    = (0:lmax)';
mat1        = mat;

for k = 1:12
    t   = find(m==k);
    tmp = 0;
    for l = 1:length(t)
        tmp = tmp + dgvar{t(l),3}(:,2);
    end

    tmp         = tmp/length(t);
    mat(:,k+1)  = tmp;
    dgvarm{k,1} = tmp;

    p           = polyfit(log10(3:26)',log10(tmp(4:27)),1);
    dgvarm{k,2} = p;
    mat1(3:end,k+1) = mat1(3:end,1).^p(1) * 10^(p(2));
    dgvarm{k,3} = mat1(:, [1,k+1]);
end

clear p mat1 mat tmp t k m

more off
