function [klm_bar, F, alpha_L, alpha_G] = regularize(klm, Qnn, S, a)
    
% REGULARIZE the given monthly spherical harmonic coefficients
%
% [klm_bar, F, alpha_L, alpha_G] = regularize(klm, N, S, a)
% 
%
% INPUT
% klm   - anomalies of the monthly spherical harmonic GRACE solutions 
%         in the SC or CS format
% Qnn   - Variance-covariance matrix where the coefficients are 
%         arranged in ITSG degree-leading order [n x n]
% S     - Time-variable Kaula rule [degree sigma^2] [n x 2]
% a     - regularization parameter
%
% OUTPUT
% klm_bar   - Filtered spherical harmnics in SC or CS
% F         - Filter matrix
% alpha_L   - Processing loss
% alpha_G   - Processing gain
%----------------------------------------------------------------------

lmax    = max(S(:,1));
ncoeff  = sum(1:lmax+1);

fprintf('Setting up signal covariance\n')
tmp = cssc2clm(S, lmax);
tmp(:, 3) = tmp(:, 3)./(2*tmp(:, 1) + 1);
tmp(:, 4) = tmp(:, 4)./(2*tmp(:, 1) + 1);
Qss = clm2itsg(tmp);


fprintf('Regularization filter\n')
F   =   diag(Qss(:,3))/(a*Qnn + diag(Qss(:, 3)));


% fprintf('Filter metrics\n')
% fprintf('\t Damping factor\n')
% alpha_  = damping_factor(F, diag(Qss(:,3)));
% fprintf('\t Processing loss\n')
% alpha_L = processing_loss(F,  diag(Qss(:,3)));

% fprintf('\t Processing gain\n')
% alpha_G = processing_gain(F,  diag(Qss(:,3)), Qnn);

fprintf('Preparing SH coefficients\n')
clm = cssc2clm(klm, lmax);
klm = clm2itsg(clm);

fprintf('Regularizing SH ...')
klm_bar = klm;
klm_bar(:, 3) = F * klm_bar(:, 3);

%% Conversion to CS format
klm_bar = sc2cs(itsg2sc(klm_bar));
fprintf(' done\n')