% Script for Filter Metrics IUGG 2019 presentation
% 15/7/2019

clc

% Preliminaries
data_path = % set data path here

addpath(genpath('/media/docs/Research/Software/Octave/SHbundle/')) % add path to SHbundle
addpath(genpath('/media/docs/Research/Software/Octave/uberall/')) % add path to uberall
addpath(genpath('/media/docs/Research/Software/gracebundle/')) % add path to gracebundle


%% Computing the Kaula rule for time-variable gravity
gettvkaula

%% Read covariance matrix
% Please write a function to read the SINEX files and insert it here.
% The output of the function must be the inverse of the Qnn matrix
% N = readsinex('sinex filename including path');
% Qnn = N\eye(size(N));


%% Regularization factor
a = [0.01, 0.1, 0.5, 1, 2, 3, 5, 8, 10, 100]';

cs_bar  = cell(size(a));

for k = 6:6
    [cs_bar{k}, B] = regularize(itsg{idx, 9}, Qnn, dgvarm{4,3},  a(k));
end

fprintf('\n\n')